/*
    Name:           location.js
    Description:    A javascript snippet to plot geolocation of current place with nearby places
    input:          rendering html file in web browser
    output:         current location map with defined nearby places
    usage:          show.html

    author:         Chandan Shaw

*/



let longitudePosition;
let latitudePosition;

var map;
var infoWindow;

//windows onload function
window.onload = function load() {
  map = document.getElementById("map");
  initMap();
  alert("Start")
}

//item list
items = ["shooping_mall", "hospital", "bus_station", "movie_theater", "taxi_stand"];

//intialization function for map api
function initMap() {

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition, Error);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }

  function showPosition(position) {
    longitudePosition = position.coords.longitude
    latitudePosition = position.coords.latitude

    pos.innerHTML = "Latitude: " + longitudePosition +
      "<br>Longitude: " + latitudePosition;

    var pyrmont = new google.maps.LatLng(latitudePosition, longitudePosition);

    map = new google.maps.Map(document.getElementById('map'), {
      center: pyrmont,
      zoom: 15,
      navigationControlOptions: {
        style: google.maps.NavigationControlStyle.SMALL
      },
      MapTypeId: google.maps.MapTypeId.ROADMAP
    });

    accuracy = position.coords.accuracy

    userPosition = {
      position: pyrmont,
      map: map,
      animation: google.maps.Animation.drop,
      draggable: true,
      title: "you are " + accuracy + " meter nearby"
    }

    let userLocation = new google.maps.Marker(userPosition)

    let transit = new google.maps.TransitLayer();
    transit.setMap(map);

    infoWindow = new google.maps.InfoWindow();

    var service = new google.maps.places.PlacesService(map);

    for (var item of items) {
      var request = {
        location: pyrmont,
        radius: 1000,
        type: item
      }

      service.nearbySearch(request, callback);
    }

  }

  function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
      for (var i = 0; i < results.length; i++) {
        var place = results[i];
        createMarker(results[i]);
      }
    }
  }

  //creating different marker for different places
  function createMarker(place) {
    var placeLoc = place.geometry.location;

    var icon = {
      url: place.icon,
      scaledSize: new google.maps.Size(20, 20),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(0, 0)
    };
          var marker = new google.maps.Marker({
            position: place.geometry.location,
            map: map,
            icon: icon
          });

          google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent("Name: " + place.name);
            infowindow.open(map, this);
          });
        }
